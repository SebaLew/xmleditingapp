import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;

public class Account implements Serializable, Comparable<Account> {
    String iban;
    String name;
    String currency;
    double balance;
    String closingDate;

    public Account() {
    }

    public Account(String iban, String name, String currency, double balance, String closingDate) {
        this.iban = iban;
        this.name = name;
        this.currency = currency;
        this.balance = balance;
        this.closingDate = closingDate;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getClosingDate() {
        return closingDate;
    }

    @Override
    public String toString() {
        return "Account{" +
                ", iban='" + iban + '\'' +
                ", name='" + name + '\'' +
                ", currency='" + currency + '\'' +
                ", balance=" + balance +
                ", closingDate=" + closingDate +
                '}';
    }

    public boolean checkIfIbanIsCorrect(String iban) {
        String mashedUpIban = iban.substring(4) + "2521" + iban.substring(2, 4);
        BigInteger ibanValidation = new BigInteger(mashedUpIban);
        BigInteger validatingNumber = new BigInteger("97");
        if (!(!iban.substring(0, 2).toUpperCase().equals("PL")
                || !(iban.length() == 28)
                || ibanValidation.mod(validatingNumber).compareTo(new BigInteger("1")) != 0))
            return false;
        else {
            return true;
                }
        }

    @Override
    public int compareTo(Account o) {
        return 0;
    }
}

