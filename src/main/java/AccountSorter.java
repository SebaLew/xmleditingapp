import jdk.vm.ci.meta.Local;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class AccountSorter {
    public static void main(String[] args) throws ParserConfigurationException,
            SAXException, IOException, ParseException {

        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
        LocalDate date = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String formattedDate = date.format(formatter);

        //Get Document Builder
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        // Load the input XML document, parse it and return an instance of the document class
        Document inputDocument = builder.parse(new File("src/main/resources/bankAccounts.xml"));

        List<Account> accounts = new ArrayList<Account>();
        NodeList nodeList = inputDocument.getDocumentElement().getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element elem = (Element) node;

                // Get the value of the ID attribute.
                String iban = node.getAttributes().getNamedItem("iban").getNodeValue();

                // Get the value of all sub-elements.
                String name = elem.getElementsByTagName("name")
                        .item(0).getChildNodes().item(0).getNodeValue();

                String currency = elem.getElementsByTagName("currency").item(0)
                        .getChildNodes().item(0).getNodeValue();

                double balance = Double.parseDouble(elem.getElementsByTagName("balance")
                        .item(0).getChildNodes().item(0).getNodeValue());

                LocalDate closingDate = LocalDate.parse(elem.getElementsByTagName("closingDate").item(0).getTextContent());

                accounts.add(new Account(iban, name, currency, balance, closingDate.format(formatter)));
            }
        }
        // remove all accounts with currency different than PLN
        accounts.removeIf(acc -> !(acc.getCurrency().toUpperCase()).equals("PLN"));

        // remove all accounts with balance below 0.00 PLN
        accounts.removeIf(account -> account.getBalance() < 0.00);

        // remove all expired accounts
        accounts.removeIf(account -> LocalDate.parse(account.getClosingDate()).isBefore(date));

        //checking if account numbers are valid for PL
        accounts.removeIf(account -> account.checkIfIbanIsCorrect(account.getIban()));

        //sorting accounts by name
        accounts.sort(Comparator.comparing(Account::getName));

        try {
            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

            Document outputDocument = documentBuilder.newDocument();
            // root element
            Element root = outputDocument.createElement("accounts");
            outputDocument.appendChild(root);

            for (Account acc:accounts) {
                // account element
                Element account = outputDocument.createElement("account");
                root.appendChild(account);

                // set an attribute to staff element
                Attr attr = outputDocument.createAttribute("iban");
                attr.setValue(acc.getIban());
                account.setAttributeNode(attr);

                //name element
                Element name = outputDocument.createElement("name");
                name.appendChild(outputDocument.createTextNode(acc.getName()));
                account.appendChild(name);

                // currency element
                Element currency = outputDocument.createElement("currency");
                currency.appendChild(outputDocument.createTextNode(acc.getCurrency()));
                account.appendChild(currency);

                // balance element
                Element balance = outputDocument.createElement("balance");
                balance.appendChild(outputDocument.createTextNode(String.valueOf(acc.getBalance())));
                account.appendChild(balance);

                // closingDate elements
                Element closingDate = outputDocument.createElement("closingDate");
                closingDate.appendChild(outputDocument.createTextNode(acc.getClosingDate()));
                account.appendChild(closingDate);
            }

            // create the xml file
            //transform the DOM Object to an XML File
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource domSource = new DOMSource(outputDocument);
            StreamResult streamResult = new StreamResult(new File("src/main/resources/bankAccountsOutput.xml"));

            transformer.transform(domSource, streamResult);

            System.out.println("Done creating XML File");

        } catch (ParserConfigurationException | TransformerException pce) {
            pce.printStackTrace();
        }
    }
}

